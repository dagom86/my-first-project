'''
#1
 a = 5
 b = a
 a = 7
 c = a + b
 print(c)
#Svar: 12

# 2
 a = 7*3+1
 print(a)

# 3
 a = 20/(4*2)
 print(a) 
 #Svar: 2.5

# 4
 a = 1
 b = a
 a = a + b
 b = a + b
 c = a + b
 print(c)

# 5
 print("Hello World!")

# 6                         // Kan förmodligen göra inputen till int på en gång.
 a = input("Skriv ett tal: ")           
 b = input("Skriv ett tal till: ")
 print(f"{int(a) + int(b)}, {int(a) - int(b)}, {int(a) * int(b)}")

# 7 Aktivera uppgift 6 innan 7 körs.
 print(f"{int(a) / int(b)}")
 print(f"{int(a) // int(b)}")

# 8 Ta in radien på en cirkel och beräkna cirkelns area. Kolla att hämta from math pi.
 a = input("Vilken radie (cm) har cirkeln? ")
 print(f"{float(a) * float(a) * float(3.14)}")

# 9 Höjd och längd av en triangel och räkna ut area. 
 a = input("Vilken höjd har triangeln? ")
 b = input("Vilken bredd har triangeln? ")
 print(f"{float(a) * float(b) / 2}")

# 10 Ekvation (x+y)*(x+y)
 x = input("Vilket värde har x (heltal)? ")
 y = input("Vilket värde har y (heltal)? ")
 print(f"{(int(x) + int(y)) * (int(x) + int(y))}")

# 11 Hypotenusan (a2 + b2 = c2)
 a = 9
 b = 7
 import math 
 c = (math.hypot(a,b))
 print(f"Hypotenuses är: {(c)}")

#12
time = 60 #en variable för timmar/sek
aTime = 455 #antal minuter att räkna från
print(f"Hours: {float(aTime) / (time) }") # ger antal timmar
print(f"Minutes: {float(aTime) % (time) }") # ger antal rest
#13
#s = int(input("Skriv in antal sekunder: "))

#m = s // 60
#t = m // 60
#d = t // 24
#r = s % 60

#print(f"{s} sekunder är {d} dagar {t} timmar {m} minuter {r} sekunder")

#14
a = 6.0
b = "Hello world"
a,b = b,a
print(a)
print(b)
'''
'''
#15
x, y, z = 10, 10, 10
print(x, y, z)

#16
celsius = float(input("Enter temperature in Celsius: "))
fahrenheit = ((celsius * 9) /5) + 32
print(f"Temperature in fahrenheit is: {float(fahrenheit)}")

#17
#from datetime import datetime
#y = int(input("Hur gammal är du? "))
#b = datetime.now().year - y
#print(f"Du föddes år {b} och blir 100 år {b +100}")

#18
num = int(input("Enter a number: "))
if(num % 2) == 0:
 print("{0} is Even number".format(num))
else:
 print("{0} is odd number".format(num))

#19
A = input("This is my best program, please enter your age: ")
print(f"Age: {int(A)}")
'''